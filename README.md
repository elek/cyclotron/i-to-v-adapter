# Current to Voltage Adapter
![](foto.jpg)
## Introduction

Cyclotron Current to Voltage is a project that consists of a hardware interface to connect to a National Instruments NI-9205 Aquisition Card.

This interface allows the current comming the Cyclotron to be measured thanks to a 430ohm resistor in series.
Up to 16 Channels can be measured using a differential input method.
![Schematic](Cyclotron_Curr_to_Volt-03.png)

## Support Files
![Schematic](Cyclotron_Curr_to_Volt.PDF)

![ni-9205_getting_started](ni-9205_getting_started.pdf)

![ni-9205_specifications](ni-9205_specifications.pdf)

